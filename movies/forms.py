from django import forms
from django.contrib.auth.models import User

from .models import Preference


class OnboardForm(forms.ModelForm):
    class Meta:
        model = Preference
        fields = ["preference_one", "preference_two", "preference_three"]


class SignupForm(forms.ModelForm):
    re_password = forms.CharField(max_length=120, widget=forms.PasswordInput)
    password = forms.CharField(max_length=120, widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = [
            "username", "first_name", "last_name", "email",
            "password", "re_password"
        ]
