from django.conf.urls import url

from . import views


app_name = 'movies'
urlpatterns = [
    url('stream', views.StreamView, name="stream"),
    url('onboard', views.OnboardView, name="onboard"),
    url('signup', views.SignupView, name="signup"),
    url('', views.GettingStartedView, name="getting_started"),
]
