from django.shortcuts import render

from django.contrib import messages
from django.contrib.auth.models import User

from .forms import OnboardForm, SignupForm
from django.core.paginator import Paginator
#from movies.models import Channel, Movie
from django.views.decorators.http import require_http_methods
from django.views.generic import CreateView, ListView, DetailView, UpdateView
from django.core.paginator import Paginator
from movies.models import Category, Movie
  
def signin(request):
    return render(request, 'signin.html')

def home(request):
    categories = Category.objects.all()
    movies = Movie.objects.all()
    return render(request, 'home.html', { 'categories': categories, 'movies': movies })


def GettingStartedView(request):
    return render(request, 'movies/getting_started.html')


def OnboardView(request):
    if request.method == "POST":
        preference_form = OnboardForm(request.POST)
        if preference_form.is_valid():
            user = User.objects.first()
            new_preference = preference_form.save(commit=False)
            new_preference.user = user
            new_preference.save()
            return render(request, 'home.html')
    else:
        return render(request, 'movies/onboarding.html')


def SignupView(request):
    if request.method == 'POST':
        form = SignupForm(request.POST)
        if form.is_valid():
            form.save()
            return render(request, 'movies/onboarding.html')
        else:
            messages.error(request, 'The form is invalid.')

        return render(request, 'movies/signup.html', {'form': form})

    else:
        form = SignupForm()
        return render(request, 'movies/signup.html', {'form': form})
