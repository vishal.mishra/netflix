from django.contrib import admin
from .models import Movie, Preference, Watchlist, Category, Upload


admin.site.register(Movie)
admin.site.register(Preference)
admin.site.register(Watchlist)
admin.site.register(Category)
admin.site.register(Upload)
